"""

Evaluate multiclass. For now a reflection of the evaluateNSD and evaluateConceptor notebooks.
"""
import csv
import numpy as np
from tqdm import tqdm
from scipy import stats
from pathlib import Path
from functools import partial
from sklearn.cluster import KMeans
from scipy.spatial import distance

import loader
from metrics import weat
from debias.conceptor import Conceptor, AND
from debias.double_hard_debias import DoubleHardDebias
from static_db import get_associated_words, database, clean
from debias.nullprojection.null_projection import NullSpaceDebias
from create_test import read_all_weat, create_intersectional_bias_test
from debias.nullprojection.classifier_training_data import generate_direction


from scipy.spatial import distance
from operator import itemgetter
import itertools
import random
import copy

# Load word embeddings
# word_embedding = loader.WE()
# E = word_embedding.load(ename='glove-wiki-gigaword-300')

def remove_frequency_component(E, min_prec=0):
    d = DoubleHardDebias()
    pca = d.my_pca(E.vecs)
    d = pca.components_[min_prec]
    wv_f = np.zeros((len(E.words), E.vecs.shape[1])).astype(float)
    w2i = {w: i for i, w in enumerate(E.words)}
    wv_mean = np.mean(np.array(E.vecs), axis=0)

    for i, w in enumerate(E.words):
        u = E.v(w)
        sub = np.zeros(u.shape).astype(float)
        # for d in D:
        sub += np.dot(np.dot(np.transpose(d), u), d)
        wv_f[w2i[w], :] = u - sub - wv_mean  # why subtract the wv_mean?

    assert E.vecs.shape == wv_f.shape
    E.vecs = wv_f
    E.model.vectors = wv_f
    return E



# R_WORDS_GENDER = database['male_names'] + database['female_names']
R_WORDS_GENDER = database['male'] + database['female']
R_WORDS_RACE = database['african_american_names'] + database['european_american_names']


database['eib_african_american_females'] = clean("aggressive, big butt, confident, dark skinned, fried chicken, overweight, promiscuous, unfeminine")
database['eib_european_american_males'] = clean("arrogant, blond, high status, intelligent, racist, rich, successful, tall")

import numpy as np
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt


class PCAComponents():
    """`PCAComponents` Class"""

    def __init__(self, E):
        """Plot the PCA principle component bar graph for some direction
        of `E` computed using a list of pairs of words.

        Args:
            E (WE class object): Word embeddings object

        """
        self.E = E

    def PlotPCA(self, pairs, title, num_components, dpi, figsize, dont_plot=False):
        """Main `PCAComponents` visualization driver function

        Args:
            pairs (list): A list of pair (tuple/list) of words. The
                        direction is computed by PCA of set of
                        differences of these words.
            title (str): title of the plot
            num_components (int): number of principal components
            dpi (int): dpi of the figures
            figsize (tuple): size of figures in (HxW)

        """

        matrix = []
        for a, b in pairs:
            center = (self.E.v(a) + self.E.v(b)) / 2
            matrix.append(self.E.v(a) - center)
            matrix.append(self.E.v(b) - center)
        matrix = np.array(matrix)
        pca = PCA(n_components=num_components)
        pca.fit(matrix)
        if dont_plot == False:
            plt.figure(figsize=figsize, dpi=dpi)
            plt.bar(range(num_components), pca.explained_variance_ratio_)
            if title is not None:
                plt.title(title)
            plt.show()

        else:
            return pca.explained_variance_ratio_

    def SinglePlotPCA(self, lists, title, num_components, dpi, figsize, dont_plot=False):
        """Main `PCAComponents` visualization driver function

        Args:
            pairs (list): A list of pair (tuple/list) of words. The
                        direction is computed by PCA of set of
                        differences of these words.
            title (str): title of the plot
            num_components (int): number of principal components
            dpi (int): dpi of the figures
            figsize (tuple): size of figures in (HxW)

        """

        matrix = []

        for a in lists:
            matrix.append(self.E.v(a))
        matrix = np.array(matrix)
        pca = PCA(n_components=num_components)
        pca.fit(matrix)
        if dont_plot == False:
            plt.figure(figsize=figsize, dpi=dpi)
            plt.bar(range(num_components), pca.explained_variance_ratio_)
            if title is not None:
                plt.title(title)
            plt.show()

        else:
            return pca.explained_variance_ratio_

    def run(self, pairs, title=None, num_components=10, dpi=300,
            figsize=(8, 5)):
        """Run the `PCAComponents` visualization

        Args:
            pairs (list): A list of pair (tuple/list) of words. The
                        direction is computed by PCA of set of
                        differences of these words.
            title (str): title of the plot
            num_components (int): number of principal components
            figsize (tuple): size of figures in (HxW)
            dpi (int): dpi of the figures

        """
        assert len(
            pairs) >= num_components, f"# pairs ({len(pairs)}) should be greater than the number of components ({num_components})."
        if type(pairs[0]) is tuple:
            self.PlotPCA(pairs, title, num_components, dpi, figsize)
        else:
            self.SinglePlotPCA(pairs, title, num_components, dpi, figsize)


def count_occurance(E, word):
    if " " in word:
        words = [w.strip() for w in word.split(" ")]
        word_count = [E.model.vocab[w].count for w in words]
        return np.mean(word_count)
    else:
        return E.model.vocab[word].count


def encode_no_debias(E, words):
    def temp(word):
        return E.v(word)

    if type(words) == str:
        return temp(words)
    else:
        words_representation = []
        for word in words:
            words_representation.append(temp(word))
        return words_representation


def evaluate_weat(encoder_function, att1, att2, x, y, p_value=False):
    att1 = encoder_function(att1)
    att2 = encoder_function(att2)
    x = encoder_function(x)
    y = encoder_function(y)
    return weat.compute_weat(None, x, y, att1, att2, p_value)


def null_projection_encoder_function(E, gender_direction):
    nsd = NullSpaceDebias()
    nsd.run(E, gender_direction)
    return partial(nsd.encode, E), nsd


def conceptor_encoder_function(E, r_words):
    cg = Conceptor()
    if r_words:
        cg.run(E, r_words)
        return partial(cg.encode, E), cg
    else:
        return cg

def double_hard_bias_encoder_function(E):
    d = DoubleHardDebias()
    E_copy = copy.deepcopy(E)
    _E = d.run(E=E_copy)
    assert _E != E
    return partial(d.encode, _E), d


def double_hard_bias_encoder_function_race(E):
    d = DoubleHardDebias(
        class_1_words=[],
        class_2_words=[],
        class_specific_words=[],
        definitional_pairs=[["black", "caucasian"], ["african", "caucasian"], ["black", "white"], ["africa", "america"], ["africa", "europe"] ],
        bias_by_projection_list=[["black", "caucasian"]]
    )
    E_copy = copy.deepcopy(E)
    _E = d.run(E=E_copy)
    assert _E != E
    return partial(d.encode, _E), d

def double_hard_bias_encoder_function_combined(E):
    d1 = DoubleHardDebias()
    E_copy1 = copy.deepcopy(E)
    _E1 = d1.run(E=E_copy1)


    d2 = DoubleHardDebias(
        class_1_words=[],
        class_2_words=[],
        class_specific_words=[],
        definitional_pairs=[["black", "caucasian"], ["african", "caucasian"], ["black", "white"], ["africa", "america"], ["africa", "europe"] ],
        bias_by_projection_list=[["black", "caucasian"]]
    )
    E_copy2 = copy.deepcopy(_E1)
    _E2 = d2.run(E=E_copy2)
    assert _E2 != _E1

    return partial(d2.encode, _E2), d2

def baseline_encoder_function(E):
    return partial(encode_no_debias, E), None


def generalized_multiple_encoder(E, encoder_list, words_to_encode):
    encoded_word_list = []
    for word in words_to_encode:
        word = word.lower()
        enc_word = encoder_list[0].encode(E, word)
        for encoder in encoder_list:
            if word in encoder.list_of_biased_entities:
                enc_word = (encoder.final_projection.dot(enc_word.T)).T
        encoded_word_list.append(enc_word)

    return encoded_word_list

def pca_conceptor(E, r_words):
    pca = PCAComponents(E)
    a = r_words
    # a = {i:E.v(i)for i in a}
    number_of_clusters = 4
    cluster_beam = []
    beam_size = 7
    minimum_cluster_size = 3
    random.shuffle(a)

    # find number_of_clusters (k) furtherst point
    max_distance = 0
    best_cluster = []
    for cluster in itertools.combinations(a, number_of_clusters):
        cluster_distance = 0
        for c1, c2 in itertools.combinations(cluster, 2):
            cluster_distance = cluster_distance + distance.cosine(E.v(c1), E.v(c2))
        if cluster_distance > max_distance:
            max_distance = cluster_distance
            best_cluster = cluster

    print(best_cluster)

    current_clusters = [[[i], 0] for i in best_cluster]
    print(current_clusters)

    final_clusters = []
    for _ in range(8):
        if len(a) < 4:
            break
        for word in a:
            new_clusters = []
            for c in current_clusters:
                if word not in c[0]:

                    new_list = c[0] + [word]
                    score = pca.SinglePlotPCA(lists=new_list, title='potato', num_components=len(new_list), dpi=300,
                                              figsize=(8, 5), dont_plot=True)[0]
                    new_clusters.append([new_list, score])
                new_clusters = sorted(new_clusters, key=itemgetter(1), reverse=True)
            current_clusters = [c for c in current_clusters if len(c[0]) > 1] + new_clusters
            # sort them by score and keep beam_size nunber of clusters
            current_clusters = sorted(current_clusters, key=itemgetter(1), reverse=True)
            if len(current_clusters) > beam_size:
                current_clusters = current_clusters[:beam_size]
        print(current_clusters[0])
        final_clusters.append(current_clusters[0])
        a = [i for i in a if i not in current_clusters[0][0]]
        print(len(a))
        #     print(a)
        curent_clusters = []
        max_distance = 0
        best_cluster = []
        for cluster in itertools.combinations(a, number_of_clusters):
            cluster_distance = 0
            for c1, c2 in itertools.combinations(cluster, 2):
                cluster_distance = cluster_distance + distance.cosine(E.v(c1), E.v(c2))
            if cluster_distance > max_distance:
                max_distance = cluster_distance
                best_cluster = cluster
        current_clusters = [[[i], 0] for i in best_cluster]

    clusters = [f[0] for f in final_clusters]
    clusters = clusters + a
    conceptor_list = []
    for word_list1 in clusters:
        #     word_list = cluster[cluster1] + cluster[cluster2]
        _, cg = conceptor_encoder_function(E, list(set(word_list1)))
        conceptor_list.append(cg)

    # conceptor_list = []
    # for key, word_list in cluster.items():
    # #     word_list = cluster[cluster1] + cluster[cluster2]
    #     _, cg = conceptor_encoder_function(E, word_list)
    #     conceptor_list.append(cg)


    cgmeta = conceptor_encoder_function(E, [])
    cgmeta.G = conceptor_list[0].G
    for c in conceptor_list[1:]:
        cgmeta.G = AND(cgmeta.G, c.G)

    cgmeta_encoder = partial(cgmeta.encode, E)

    return cgmeta_encoder


def k_means_conceptor(E, r_words, n_clusters=6):
    data_matrix = np.asarray([E.v(d) for d in r_words])
    kmeans = KMeans(n_clusters=n_clusters, random_state=0).fit(data_matrix)
    cluster = {}
    for d in r_words:
        cluster_id = kmeans.predict(E.v(d).reshape(1, -1))[0]
        if cluster_id in cluster.keys():

            cluster[cluster_id] = cluster[cluster_id] + [d]
        else:
            cluster[cluster_id] = [d]

    keys = [i for i in cluster.keys()]

    cluster_combination = []
    for i in keys:
        max_distance = 0
        pair = [i, i]
        for j in keys:
            dis = distance.cosine(kmeans.cluster_centers_[i], kmeans.cluster_centers_[j])
            if max_distance < dis:
                max_distance = dis
                pair = [i, j]
        cluster_combination.append(pair)

    # cluster_combination = cluster_combination[:3]

    conceptor_list = []
    for cluster1, cluster2 in cluster_combination:
        word_list = cluster[cluster1] + cluster[cluster2]
        _, cg = conceptor_encoder_function(E, word_list)
        conceptor_list.append(cg)


    cgmeta = conceptor_encoder_function(E, [])
    cgmeta.G = conceptor_list[0].G
    for c in conceptor_list[1:]:
        cgmeta.G = AND(cgmeta.G, c.G)

    cgmeta_encoder = partial(cgmeta.encode, E)

    return cgmeta_encoder


def get_correlation(E, encoder,atts1, atts2, X, Y):
    max_score = 0
    pairs_and_score = []
    # pair = []
    for att1 in atts1:
        for att2 in atts2:
            score = evaluate_weat(encoder, [att2], [att1], X,
                                  Y)
            pairs_and_score.append([att1, att2, abs(score), count_occurance(E, att1), count_occurance(E, att2),
                                    max(count_occurance(E, att1), count_occurance(E, att2))])
            if abs(score) > abs(max_score):
                max_score = score
                pair = [att1, att2]
    score = [s[2] for s in pairs_and_score]
    avg_count = [np.mean([s[3], s[4]]) for s in pairs_and_score]
    max_count = [s[5] for s in pairs_and_score]
    print(stats.spearmanr(score, max_count))



def evaluate_weat_from_iats(encoder:partial, encoder_info: str, eval_p: bool = False):
    """evaluates weat over the dataset provided by Assessing Social and Intersectional Biases inContextualized Word Representations"""

    # tests = read_all_weat()
    tests = create_intersectional_bias_test() + read_all_weat()
    saved_results = []
    for t in tqdm(tests):
        temp = {}
        results = evaluate_weat(encoder, t['attr1'], t['attr2'], t['targ1'], t['targ2'], eval_p)
        if eval_p:
            score = results[0]
            p_value = results[1]
        else:
            score = results
            p_value = -10.0
        temp['result'] = score
        temp['p_value'] = p_value
        temp['encoder_info'] = encoder_info
        temp['name_of_test'] = t['name_of_test']
        saved_results.append(temp)
    return saved_results


def write_results_to_csv(data:list, file_name:Path):
    """

    :param data: info in the form of list of dict. For example [{'a':1,'b':1}, {'a':2,'b':6}]
    :file_name: file path to store results
    :return: confirmation string
    """
    keys = data[0].keys()
    with open(file_name, 'w', newline='')  as output_file:
        dict_writer = csv.DictWriter(output_file, keys)
        dict_writer.writeheader()
        dict_writer.writerows(data)
    return f"file saved at location {file_name}"


def projections(vec, direction):
    return np.dot(vec, direction) / np.linalg.norm(direction)


def selective_conceptor_encoder(E, directions, conceptors_partial, threshold, words):
    """direction and conceptors in same order
    [gender_direction, race_direction] -> [gender_conceptor, race_conceptor]
    """

    def temp_one(word):
        direction_component = [abs(projections(E.v(word), d)) for d in directions[:-1]]
        index = np.argmax(direction_component)
        conceptor_partial = conceptors_partial[index]
        return conceptor_partial(word)

    def temp(word):
        """assumes third one is common"""
        direction_component = [abs(projections(E.v(word), d)) for d in directions[:-1]]
        direction_component[0] = direction_component[0] - 0.46  # 0.46 #0.36
        direction_component[1] = direction_component[1] - 0.90  # 0.90 #0.38
        if abs(direction_component[0] - direction_component[1]) < threshold:
            conceptor_partial = conceptors_partial[-1]
        else:
            index = np.argmax(direction_component)
            conceptor_partial = conceptors_partial[index]
        return conceptor_partial(word)

    if type(words) == str:
        return temp(word=words)

    encoded_words = []
    for w in words:
        encoded_words.append(temp(word=w))

    return encoded_words


def m_selective_conceptor_encoder(E, distance_partial, conceptors_partial, threshold, words):
    """direction and conceptors in same order
    [gender_direction, race_direction] -> [gender_conceptor, race_conceptor]
    """

    def temp_one(word):
        direction_component = [d(E.v(word)) for d in distance_partial[:-1]]
#         direction_component = [d(E.v(word)) for d in distance_partial]
#         direction_component[0] = direction_component[0]
#         direction_component[1] = direction_component[1]
        index = np.argmin(direction_component)
        conceptor_partial = conceptors_partial[index]
        return conceptor_partial(word)

    def temp(word):
        """assumes third one is common"""
        direction_component = [abs(d(E.v(word))) for d in distance_partial[:-1]]
#         direction_component[0] = (direction_component[0] - 0.36) / .30  # 0.35#0.46 #0.36
#         direction_component[1] = (direction_component[1] - 0.38) / .33  # 0.41#0.90 #0.38
        if abs(direction_component[0] - direction_component[1]) < threshold:
            conceptor_partial = conceptors_partial[-1]
        else:
            index = np.argmin(direction_component)
            conceptor_partial = conceptors_partial[index]
        return conceptor_partial(word)

    if type(words) == str:
        return temp(word=words)

    encoded_words = []
    for w in words:
        encoded_words.append(temp(word=w))

    return encoded_words

def mahalanobis_distance(mean_cov_matrix, inv_cov_matrix, x_vec):
    x_mu = x_vec - mean_cov_matrix
    left_term = np.dot(x_mu, inv_cov_matrix)
    mahal = np.dot(left_term, x_mu.T)
    return mahal

def construct_data_matrix(E, pairs):
    matrix = []
    for i,j in pairs:
        matrix.append(E.v(i))
        matrix.append(E.v(j))
    return np.array(matrix)



# eval_get_correlation(E, cge, cre, cgre, npge, npre, npgre, bge, bre, bgre)
# eval_weat(cge, cre, cgre, npge, npre, npgre, bge, bre, bgre)


# print(f"Z is 'all_male_names' + 'all_female_names' " )
#
# meta_names_male = database['male_names'] + database['african_american_male_names'] + database['european_american_male_names']
# meta_names_female = database['female_names'] + database['african_american_female_names'] + database['european_american_female_names']
#
# meta_names_male, meta_names_female = list(set(meta_names_male)), list(set(meta_names_female))
# database['meta_male_names'] = meta_names_male
# database['meta_female_names'] = meta_names_female
#
# R_WORDS_GENDER =  database['male'] + database['female']

if False:
    print(database['male'])
    print(database['female'])
    print(database['male_2'])
    print(database['female_2'])
    print(get_associated_words('math'))
    print(get_associated_words('arts'))
    for n_clusters in range(2,10):
        cge, cg = conceptor_encoder_function(E, R_WORDS_GENDER)
        cgr = conceptor_encoder_function(E, [])
        # adding the transformation matrix. The transformation matrix for conceptor is represented by G in the code base.
        cgr.G = AND(cg.G, AND(cg.G, cg.G))

        cgre = partial(cgr.encode, E)

        kcge = k_means_conceptor(E, R_WORDS_GENDER, n_clusters=n_clusters)
        # finer_evalaute(bge, database)

        results_conceptor = evaluate_weat_from_iats(encoder=cge, encoder_info='conceptor (male female)', eval_p=True)
        write_results_to_csv(data=results_conceptor, file_name=Path('../results/conceptor_gender.csv'))
        break

        results_kmeans = evaluate_weat_from_iats(encoder=kcge, encoder_info='kmeans conceptor (male female)', eval_p=True)
        # write_results_to_csv(data=results_kmeans, file_name=Path('../results/kmeans_conceptor_gender.csv'))

        kmeans_winner = []
        conceptor_winner = []

        for i,j in zip(results_kmeans, results_conceptor):
            if i['result'] >= j['result']:
                kmeans_winner.append(1)
            else:
                conceptor_winner.append(1)

        # print(len(kmeans_winner), len(conceptor_winner))

        print(f"conceptor weat is higher for {len(conceptor_winner)}, while kmeans is  higher for {len(kmeans_winner)} and n_cluster is {n_clusters}")
#


def evaluate_all_encoders(encoders, encodern, filename):



    # encoders = [dre, dgre]
    # encodern = ["dre", "dgge"]
    results = []

    for enc, enc_name in tqdm(zip(encoders, encodern)):
        r = evaluate_weat_from_iats(encoder=enc, encoder_info=enc_name, eval_p=True)
        results.append(r)

    ## [ [[],[],[]], [[],[],[]], [[],[],[]]] -> [[], [], [], []]

    reformat_results = []
    for items in zip(*results):
        temp = {}
        temp['name_of_test'] = items[0]['name_of_test']
        for i in items:
            if i['p_value'] < 0.05:
                temp[i['encoder_info']] = i['result']
            else:
                temp[i['encoder_info']] = 0.0
            # temp[i['encoder_info'] + '_pvalue'] = i['p_value']
        reformat_results.append(temp)
    write_results_to_csv(data=reformat_results, file_name=Path(filename))

# evaluate_all_encoders()
#
# res = []
# for i in range(1,12):
#     kcge = k_means_conceptor(E, R_WORDS_RACE + R_WORDS_GENDER, n_clusters=i)
#     results = evaluate_weat_from_iats(encoder=kcge, encoder_info='enc_name', eval_p=True)
#     numper_of_non_p = 0
#     for r in results:
#         if r['p_value'] > 0.05:
#             numper_of_non_p = numper_of_non_p + 1
#     print(i, numper_of_non_p)
#     res.append([i,numper_of_non_p])
#
# print(res)

#
# results = evaluate_weat_from_iats(encoder=kcge_pca, encoder_info='enc_name', eval_p=True)
# number_of_non_p = 0
# print(results)
# for r in results:
#     if r['p_value'] > 0.05:
#         number_of_non_p = number_of_non_p + 1
# # print(i, numper_of_non_p)
# # res.append([i,numper_of_non_p])
#
# print(number_of_non_p)

# for index, item in enumerate(results[0]):
#     temp = {}
#     for r in results:
#         temp[r['encoder_info']] = r[index]['encoder_info']

import numpy as np
from numpy import diag, inf
from numpy import copy, dot
from numpy.linalg import norm


class ExceededMaxIterationsError(Exception):
    def __init__(self, msg, matrix=[], iteration=[], ds=[]):
        self.msg = msg
        self.matrix = matrix
        self.iteration = iteration
        self.ds = ds

    def __str__(self):
        return repr(self.msg)


def nearcorr(A, tol=[], flag=0, max_iterations=100, n_pos_eig=0,
             weights=None, verbose=False,
             except_on_too_many_iterations=True):
    """
    X = nearcorr(A, tol=[], flag=0, max_iterations=100, n_pos_eig=0,
        weights=None, print=0)
    Finds the nearest correlation matrix to the symmetric matrix A.
    ARGUMENTS
    ~~~~~~~~~
    A is a symmetric numpy array or a ExceededMaxIterationsError object
    tol is a convergence tolerance, which defaults to 16*EPS.
    If using flag == 1, tol must be a size 2 tuple, with first component
    the convergence tolerance and second component a tolerance
    for defining "sufficiently positive" eigenvalues.
    flag = 0: solve using full eigendecomposition (EIG).
    flag = 1: treat as "highly non-positive definite A" and solve
    using partial eigendecomposition (EIGS). CURRENTLY NOT IMPLEMENTED
    max_iterations is the maximum number of iterations (default 100,
    but may need to be increased).
    n_pos_eig (optional) is the known number of positive eigenvalues
    of A. CURRENTLY NOT IMPLEMENTED
    weights is an optional vector defining a diagonal weight matrix diag(W).
    verbose = True for display of intermediate output.
    CURRENTLY NOT IMPLEMENTED
    except_on_too_many_iterations = True to raise an exeption when
    number of iterations exceeds max_iterations
    except_on_too_many_iterations = False to silently return the best result
    found after max_iterations number of iterations
    ABOUT
    ~~~~~~
    This is a Python port by Michael Croucher, November 2014
    Thanks to Vedran Sego for many useful comments and suggestions.
    Original MATLAB code by N. J. Higham, 13/6/01, updated 30/1/13.
    Reference:  N. J. Higham, Computing the nearest correlation
    matrix---A problem from finance. IMA J. Numer. Anal.,
    22(3):329-343, 2002.
    """

    # If input is an ExceededMaxIterationsError object this
    # is a restart computation
    if (isinstance(A, ExceededMaxIterationsError)):
        ds = copy(A.ds)
        A = copy(A.matrix)
    else:
        ds = np.zeros(np.shape(A))

    eps = np.spacing(1)
    if not np.all((np.transpose(A) == A)):
        raise ValueError('Input Matrix is not symmetric')
    if not tol:
        tol = eps * np.shape(A)[0] * np.array([1, 1])
    if weights is None:
        weights = np.ones(np.shape(A)[0])
    X = copy(A)
    Y = copy(A)
    rel_diffY = inf
    rel_diffX = inf
    rel_diffXY = inf

    Whalf = np.sqrt(np.outer(weights, weights))

    iteration = 0
    while max(rel_diffX, rel_diffY, rel_diffXY) > tol[0]:
        iteration += 1
        if iteration > max_iterations:
            if except_on_too_many_iterations:
                if max_iterations == 1:
                    message = "No solution found in "\
                              + str(max_iterations) + " iteration"
                else:
                    message = "No solution found in "\
                              + str(max_iterations) + " iterations"
                raise ExceededMaxIterationsError(message, X, iteration, ds)
            else:
                # exceptOnTooManyIterations is false so just silently
                # return the result even though it has not converged
                return X

        Xold = copy(X)
        R = X - ds
        R_wtd = Whalf*R
        if flag == 0:
            X = proj_spd(R_wtd)
        elif flag == 1:
            raise NotImplementedError("Setting 'flag' to 1 is currently\
                                 not implemented.")
        X = X / Whalf
        ds = X - R
        Yold = copy(Y)
        Y = copy(X)
        np.fill_diagonal(Y, 1)
        normY = norm(Y, 'fro')
        rel_diffX = norm(X - Xold, 'fro') / norm(X, 'fro')
        rel_diffY = norm(Y - Yold, 'fro') / normY
        rel_diffXY = norm(Y - X, 'fro') / normY

        X = copy(Y)

    return X


def proj_spd(A):
    # NOTE: the input matrix is assumed to be symmetric
    d, v = np.linalg.eigh(A)
    A = (v * np.maximum(d, 0)).dot(v.T)
    A = (A + A.T) / 2
    return(A)


if __name__ == '__main__':
    # Load word embeddings
    word_embedding = loader.WE()
    E = word_embedding.load(ename='glove-wiki-gigaword-300')
    E = remove_frequency_component(E=E, min_prec=1)

    # remove the freqency component from everything (take it as 1)

    print("WE loaded")


    # gender debias, race debias  for double hard debias
    # dge, _ = double_hard_bias_encoder_function(E)
    # dre, _ = double_hard_bias_encoder_function_race(E)
    # dgre, _ = double_hard_bias_encoder_function_combined(E)


    #
    # # gender debias, race debias, and for null space projection -> npgre (Null space Projection Gender Race Encoder)
    # gender_direction = generate_direction(E, False, [[a, b] for a, b in zip(database['male'], database['female'])])
    # npge, npg = null_projection_encoder_function(E, gender_direction)
    # # race_list_1 = [[a, b] for a, b in zip(database['european_american_concept'], database['african_american_concept'])]
    # race_list_2 = [[a, b] for a, b in zip(database['african_american_names'], database['european_american_names'])]
    # race_list = race_list_2
    # race_direction = generate_direction(E, False, race_list)
    # npre, npr = null_projection_encoder_function(E, race_direction)
    # # npgre = partial(generalized_multiple_encoder, E, [npg, npr])
    # npgre, npgr = null_projection_encoder_function(E, [gender_direction, race_direction])

    # # gender debias, race debias, and for conceptor -> cge, cre, cgre(Conceptor Gender Race Encoder)
    cge, cg = conceptor_encoder_function(E, R_WORDS_GENDER)
    cre, cr = conceptor_encoder_function(E, R_WORDS_RACE)
    cgr = conceptor_encoder_function(E, [])
    # adding the transformation matrix. The transformation matrix for conceptor is represented by G in the code base.
    cgr.G = AND(cg.G, cr.G)
    cgre = partial(cgr.encode, E)

    # selective_conceptor_encoder(E, [gender_direction, race_direction], [cge, cre], ['dog', 'cat'])
    gender_direction = generate_direction(E, False, [[a, b] for a, b in zip(database['male'], database['female'])])
    race_direction = generate_direction(E, False, [[a, b] for a, b in zip(database['african_american_names'],
                                                                          database['european_american_names'])])

    # encoders = []
    # encodern = []
    # for i in [0.05, 0.10, 0.15, 0.20, 0.25, 0.30]:
    #     encoders.append(partial(selective_conceptor_encoder, E, [gender_direction, race_direction, ""], [cge, cre, cgre], i))
    #     encodern.append(str(i))
    sce = partial(selective_conceptor_encoder, E, [gender_direction, race_direction, ""], [cge, cre, cgre], 0.5)

    gender_matrix = construct_data_matrix(E, [[a, b] for a, b in zip(database['male'], database['female'])])
    covariance_gender_matrix = np.cov(gender_matrix.T)  # 300*300
    covariance_gender_matrix = nearcorr(covariance_gender_matrix)
    inv_covmat_gender = np.linalg.inv(covariance_gender_matrix)
    mean_gender_matrix = np.mean(gender_matrix)

    race_matrix = construct_data_matrix(E, [[a, b] for a, b in zip(database['african_american_names'],
                                                                   database['european_american_names'])])
    covariance_race_matrix = np.cov(race_matrix.T)  # 300*300
    covariance_race_matrix = nearcorr(covariance_race_matrix)
    inv_covmat_race = np.linalg.inv(covariance_race_matrix)
    mean_race_matrix = np.mean(race_matrix)

    comb_matrix = construct_data_matrix(E, [[a, b] for a, b in
                                            zip(database['african_american_names'] + database['male'],
                                                database['european_american_names'] + database['female'])])
    covariance_comb_matrix = np.cov(comb_matrix.T)  # 300*300
    covariance_comb_matrix = nearcorr(covariance_comb_matrix)
    # covariance_comb_matrix = MinCovDet().fit(comb_matrix).covariance_
    inv_covmat_comb = np.linalg.inv(covariance_comb_matrix)
    mean_comb_matrix = np.mean(comb_matrix, axis=0)


    gender_distance_partial = partial(mahalanobis_distance, mean_gender_matrix, inv_covmat_gender)
    race_distance_partial = partial(mahalanobis_distance, mean_race_matrix, inv_covmat_race)
    comb_distance_partial = partial(mahalanobis_distance, mean_comb_matrix, inv_covmat_comb)

    mce = partial(m_selective_conceptor_encoder, E, [gender_distance_partial, race_distance_partial, comb_distance_partial],
                  [cge, cre, cgre_and], 1.0)


    # gender debias, race debias, and for baseline -> npgre (Null space Projection Gender Race Encoder)
    bge = partial(encode_no_debias, E)

    # kcge_8 = k_means_conceptor(E, R_WORDS_RACE + R_WORDS_GENDER, n_clusters=8)
    # kcge_11 = k_means_conceptor(E, R_WORDS_RACE + R_WORDS_GENDER, n_clusters=11)
    # kcge_pca = pca_conceptor(E, R_WORDS_RACE + R_WORDS_GENDER)


    # encoders = [bge, cge, cre, cgre, npge, npre, npgre, kcge_8, kcge_11, kcge_pca]
    # encodern = ["bge", "cgr", "cre", "cgre", "npge", "npre", "npgre", "kcge_8", "kcge_11", "kcge_pca"]
    encoders = [ mce, cgre]
    encodern = ["mce", "cgre"]

    filename = '../results/intersectional_bias_part_near_corr_temp.csv'

    evaluate_all_encoders(encoders=encoders, encodern=encodern, filename=filename)