"""
Debias via conceptor. The file is an adaptation/copy of
https://github.com/thaleaschlender/An-Evaluation-of-Multiclass-Debiasing-Methods-on-Word-Embeddings/blob/main/An%20Evaluation%20of%20Multiclass%20Debiasing%20Methods%20on%20Word%20Embeddings(GloVe).ipynb

Conceptor Debiasing of Word Representations Evaluated on WEAT - Karve et al.
"""


from metrics import weat
from loader import WE
import static_db

import numpy as np
from tqdm import tqdm
from typing import List, Dict, Union

class Conceptor:
    
    def __init__(self):
        self.G = None

    @staticmethod
    def find_negated_conceptor(Z, aperture = 10):
        """
        Find conceptor to debias with

        Note that Z should be no_of_words*emb_dim ... For example, if there are are  10 words with each being 50 dim.
        Then Z.shape() is (10,50).

        Note that it is not sure if k in the paper represents emb_dim. But the code below assumes k to be emn_dim.
        """
        Z = Z.transpose() # 50*10
        k = len(Z) # 50*10 (10 words of 50 dim)
        Z_Trans = Z.transpose() # 10*50
        C_1 = (1/k) * np.matmul(Z,Z_Trans) # 50*50
        temp = (aperture**(-2)) * np.identity(Z.shape[0]) # 50*50
        C_2 = np.linalg.inv(np.add(C_1, temp)) # 50*50
        C = np.matmul(C_1,C_2) # 50*50
        G = np.subtract(np.identity(Z.shape[0]), C) # 50 * 50
        return G

    def encode(self, E, words: Union[List,str]):
        """G = emb_dim*emb_dim and E.v = emb_dim,1"""

        def temp(word):
            word_emb = E.v(word)
            word_emb = word_emb / np.linalg.norm(word_emb)
            return np.matmul(word_emb, self.G)

        if type(words) == str:
            return temp(words)
        encoded_words = []
        for word in words:
            encoded_words.append(temp(word))
        return encoded_words


    def run(self, E, words:List, aperture = 10):
        words_embedded = np.array([E.v(w) for w in words])
        G = self.find_negated_conceptor(words_embedded, aperture)
        self.G = G

    def evaluate_weat_for_one_class(self, E, eval_dict: Dict, p_eval=True):
        """
        :param eval_dict:
        :return:

        Example of a eval_dict for class religion
        {
            ('islam_words', 'judaism_words'): [('likeable', 'unlikeable'), ('competent', 'incompetent')],
            ('islam_words', 'christianity_words'): [('likeable', 'unlikeable'), ('competent', 'incompetent')],
            ('judaism_words', 'christianity_words'): [('likeable', 'unlikeable'), ('competent', 'incompetent')]
        }
        """
        p_value , bias_value = [], []
        for categories, attributes in tqdm(eval_dict.items()):
            X = self.encode(E, static_db.get_associated_words(categories[0]))
            Y = self.encode(E, static_db.get_associated_words(categories[1]))
            for att1, att2 in attributes:
                A = self.encode(E, static_db.get_associated_words(att1))
                B = self.encode(E, static_db.get_associated_words(att2))
                if p_eval:
                    b, p = weat.compute_weat(None, X, Y, A, B, p_eval)
                    p_value.append(p)
                    bias_value.append(b)
                else:
                    b = weat.compute_weat(None, X, Y, A, B, p_eval)
                    bias_value.append(b)

        if p_eval:
            return np.mean(bias_value), np.mean(p_value)
        else:
            return np.mean(bias_value)


def AND(C, B, out_mode="simple", tol=1e-14):
    """
    Compute AND Operation of two conceptor matrices

    @param C: a conceptor matrix
    @param B: another conceptor matrix
    @param out_mode: output mode ("simple"/"complete")
    @param tol: adjust parameter for almost zero

    @return C_and_B: C AND B
    @return U: eigen vectors of C_and_B
    @return S: eigen values of C_and_B
    """

    dim = C.shape[0]

    UC, SC, _ = np.linalg.svd(C) # eigen vector and eigen value of C
    UB, SB, _ = np.linalg.svd(B) # eigen vector and eigen value of B

    num_rank_C = np.sum((SC > tol).astype(float))
    num_rank_B = np.sum((SB > tol).astype(float))

    UC0 = UC[:, int(num_rank_C):]
    UB0 = UB[:, int(num_rank_B):]

    W, sigma, _ = np.linalg.svd(UC0.dot(UC0.T) + UB0.dot(UB0.T))
    num_rank_sigma = np.sum((sigma > tol).astype(float))
    Wgk = W[:, int(num_rank_sigma):]

    C_and_B = Wgk.dot(
        np.linalg.inv(Wgk.T.dot(np.linalg.pinv(C, tol) + np.linalg.pinv(B, tol) - np.eye(dim)).dot(Wgk))).dot(Wgk.T)

    if out_mode == "complete":
        U, S, _ = np.linalg.svd(C_and_B)
        return C_and_B, U, S
    else:
        return C_and_B

def evaluate_weat():
    """put this function somewhere else"""
    word_embedding = WE()
    E = word_embedding.load(ename='glove-wiki-gigaword-50')
    r_words = static_db.get_associated_words('male_names') + static_db.get_associated_words('female_names')
    conceptor = Conceptor()
    conceptor.run(E, r_words)
    attribute_set = static_db.get_associated_words
    math = conceptor.encode(E, attribute_set('math'))
    art = conceptor.encode(E, attribute_set('art'))
    male = conceptor.encode(E, attribute_set('male_names'))
    female = conceptor.encode(E, attribute_set('female_names'))
    print(f"weat score of for male, female, math, art is {weat.compute_weat(None, male, female, math, art)}")


if __name__ == '__main__':
    evaluate_weat()
    # word_embedding = WE()
    # E = word_embedding.load(ename='glove-wiki-gigaword-50')
    # r_dict = static_db.get_religion_words(by='karve')
    # r_words = list(set([v for key,value in r_dict.items() for v in value])) # Z set in paper
    # conceptor = Conceptor()
    # conceptor.run(E, r_words)
    # bias = conceptor.evaluate_weat_for_one_class(E, static_db.religion_eval, False)
    # print(f"bias after conceptor debias is {bias}")