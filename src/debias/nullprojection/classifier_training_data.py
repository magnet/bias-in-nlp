"""
Generates classifier training data.
Algorithm:
    1) Calculate gender vector
    2) Find most similar set
        2a) Gender vector
        2b) - Gender vector
        2c) Everything else
"""
# from src import utils
# from src import static_db
# from src import loader
import utils
import loader
from static_db import database


import numpy as np
from typing import List
from sklearn.utils import shuffle


def generate_direction(E, via_pca:bool= True, word_list:List = [['she','he']]):
    """
    The actual work horse
    :param via_pca:
    :param number_of_samples:
    :param word_list: A list of list (generally a list represents a pair) which primalry differes in the direction to
    be calculated. For example in case of calculating gender the word_list could be [['she','he']]
    :return:
    """
    if len(word_list[0]) == 2:
        if via_pca:
            direction = utils.get_g_two_components(E = E, definitional=word_list)
        else:
            # definitional = static_db.get_gender_definitional()
            temp = []
            for words in word_list:
                temp.append(E.v(words[0])-E.v(words[1]))
            direction = np.mean(np.array(temp), 0)
            # gender_direction = E.v('she') - E.v('he') # @TODO: Check if she-he or he-she
    else:
        raise NotImplementedError
    return direction


def project_on_gender_subspaces(gender_vector, model, n=7500):
    """
    >> Directly copied from https://github.com/shauli-ravfogel/nullspace_projection

    @TODO: Find a better way to calculate set. The training data looks weird.
    """
    group1 = model.similar_by_vector(gender_vector, topn=n, restrict_vocab=None)
    group2 = model.similar_by_vector(-gender_vector, topn=n, restrict_vocab=None)

    all_sims = model.similar_by_vector(gender_vector, topn=len(model.vectors), restrict_vocab=None)
    eps = 0.03
    idx = [i for i in range(len(all_sims)) if abs(all_sims[i][1]) < eps]
    samp = set(np.random.choice(idx, size=n))
    neut = [s for i, s in enumerate(all_sims) if i in samp]
    return group1, group2, neut


def get_vectors(E, entity_list):
    entity_vectors = [E.v(entity) for entity in entity_list]
    return np.array(entity_vectors)

def generate_x_and_y(E, group1, group2, neutral):
    group1_ent, group1_score = list(zip(*group1))
    group2_ent, group2_score = list(zip(*group2))
    neutral_ent, neutral_score = list(zip(*neutral))
    n = max(len(group1_ent),len(group2_ent),len(neutral_ent))

    group1_vec, group2_vec, neutral_vec = get_vectors(E, group1_ent[:n]), \
                                          get_vectors(E, group2_ent[:n]), get_vectors(E, neutral_ent[:n])
    group1_ent, group1_score, group1_vec = shuffle(group1_ent, group1_vec, group1_vec)
    group2_ent, group2_score, group2_vec = shuffle(group2_ent, group2_vec, group2_vec)
    neutral_ent, neutral_score, neutral_vec = shuffle(neutral_ent, neutral_vec, neutral_vec)

    group1_y = np.ones(group1_vec.shape[0])
    group2_y = np.zeros(group2_vec.shape[0])
    neutral_y = -np.ones(neutral_vec.shape[0])

    X_ent = np.concatenate((group1_ent,group2_ent,neutral_ent), axis=0)
    X_vec = np.concatenate((group1_vec, group2_vec, neutral_vec), axis=0)
    Y = np.concatenate((group1_y, group2_y, neutral_y), axis=0)

    X_ent, X_vec, Y = shuffle(X_ent, X_vec, Y)  #np.concatenate((X_vec,X_vec),axis=0).shape
    return X_ent, X_vec, Y


def generate_x_and_y_multiple(E, group_set):
    """
    The aim of the function is to generalize generate_x_and_y to more than one group i.e.
    group_set = [(group1,group2,neutral), (group1,group2,neutral)] -> where the first set is for gender and
    second set is for race.
    :param embedding:
    :param group_set:
    :return:
    """
    final_x_ent, final_x_vec, final_y = [], [], []
    for index, group in enumerate(group_set):
        group1, group2, neutral = group
        X_ent, X_vec, Y = generate_x_and_y(E, group1, group2, neutral)
        Y = Y*(index+1)
        final_x_ent.append(X_ent)
        final_x_vec.append(X_vec)
        final_y.append(Y)
    final_x_ent, final_x_vec, final_y = np.concatenate(final_x_ent, 0), np.concatenate(final_x_vec, 0),\
                                        np.concatenate(final_y, 0)
    final_x_ent, final_x_vec, final_y = shuffle(final_x_ent, final_x_vec, final_y)
    return final_x_ent, final_x_vec, final_y


def temp():
    word_embedding = loader.WE()
    E = word_embedding.load(ename='glove-wiki-gigaword-50')
    print("WE loaded")

    gender_direction = generate_direction(E, False, [['she', 'he']])
    print(gender_direction.shape)

    race_list = [[a, b] for a, b in zip(database['european_american_concept'], database['african_american_concept'])]
    race_direction = generate_direction(E, False, race_list)

    print(race_direction.shape)

    directions = [gender_direction, race_direction]
    group_set = [project_on_gender_subspaces(direction, E.model) for direction in directions]

    X_ent, X, Y = generate_x_and_y_multiple(E, group_set)

    print(f"shape of X_ent is {X_ent.shape}")
    print(f"shape of X is {X.shape}")
    print(f"shape of Y is {Y.shape}")



if __name__ == '__main__':

    # temp()
    # raise NotImplementedError
    word_embedding = loader.WE()
    E = word_embedding.load(ename='glove-wiki-gigaword-50')
    print("WE loaded")

    #
    # filter = loader.FilterWordEmbeddings(E.words, E.vecs, E.model)
    # E.words, E.vecs, E.model = filter.filter_word_embeddings(keep_name=False, keep_gendered=False,
    #                                                          keep_punctuations=False,
    #                                                          keep_digits=False)
    # E.reindex()

    gender_direction = generate_direction(E, False, [['she', 'he']])
    print(gender_direction.shape)

    assert np.array_equal(gender_direction, E.v('she') - E.v('he'))

    print(f"the most similar word vectors are {E.model.similar_by_vector(gender_direction, topn=10)}")
    print(f"the most dis-similar word vectors are {E.model.similar_by_vector(-gender_direction, topn=10)}")

    group1, group2, neutral = project_on_gender_subspaces(gender_direction, E.model)
    print(f"top group 1 words are {group1[:20]}")
    print(f"top group 2 words are {group2[:20]}")
    print(f"top neutral words are {neutral[:20]}")

    X_ent, X, Y = generate_x_and_y(E, group1, group2, neutral)

    print(f"shape of X_ent is {X_ent.shape}")
    print(f"shape of X is {X.shape}")
    print(f"shape of Y is {Y.shape}")