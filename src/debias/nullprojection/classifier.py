"""

Various Classifier


TODO: Write an interface for classifier such that any of the classifier can be trained
    : Write specific instance of the classifier
    : Write the main code (expand on the details later)
"""


from abc import ABC

from sklearn.linear_model import SGDClassifier
from sklearn.svm import LinearSVC
from config import sgd_classifier, linear_svc
from .classifier_training_data import generate_direction, project_on_gender_subspaces, generate_x_and_y
import loader
from sklearn.base import clone

class Classifiers(ABC):

    def train(self, X_train, Y_train, X_dev, Y_dev):
        """

        :param X_train: numpy array representing train input
        :param Y_train: numpy array representing train output
        :param X_dev:numpy array representing dev input
        :param Y_dev:numpy array representing dev output
        :return: Trained Classifier
        """
        raise NotImplementedError

    def get_weights(self):
        raise NotImplementedError

class SklearnClassifier(Classifiers):

    def __init__(self, classifier_type:str = 'SGDClassifier'):
        if classifier_type.lower() == 'sgdclassifier':
            self.model = SGDClassifier(**sgd_classifier)
        elif classifier_type.lower() == 'linearsvc':
            self.model = LinearSVC(**linear_svc)
        else:
            raise NotImplementedError

    def train(self, X_train, Y_train, X_dev, Y_dev):
        """
        :param X_train:
        :param Y_train:
        :param X_dev:
        :param Y_dev:
        :return:
        """

        self.model.fit(X_train,Y_train)
        return self.model.score(X_dev, Y_dev)

    def get_weights(self):
        return self.model.coef_

    def reset(self):
        self.model = clone(self.model)

if __name__ == '__main__':

    word_embedding = loader.WE()
    E = word_embedding.load(ename='glove-wiki-gigaword-50')
    print("WE loaded")

    #
    # filter = loader.FilterWordEmbeddings(E.words, E.vecs, E.model)
    # E.words, E.vecs, E.model = filter.filter_word_embeddings(keep_name=False, keep_gendered=False,
    #                                                          keep_punctuations=False,
    #                                                          keep_digits=False)
    # E.reindex()

    gender_direction = generate_direction(E, False)
    print(gender_direction.shape)

    print(f"the most similar word vectors are {E.model.similar_by_vector(gender_direction, topn=10)}")
    print(f"the most dis-similar word vectors are {E.model.similar_by_vector(-gender_direction, topn=10)}")

    group1, group2, neutral = project_on_gender_subspaces(gender_direction, E.model)
    print(f"top group 1 words are {group1[:20]}")
    print(f"top group 2 words are {group2[:20]}")
    print(f"top neutral words are {neutral[:20]}")

    X_ent, X, Y = generate_x_and_y(E, group1, group2, neutral)

    print(f"shape of X_ent is {X_ent.shape}")
    print(f"shape of X is {X.shape}")
    print(f"shape of Y is {Y.shape}")
    split = int(len(X)*.80)
    X_train, Y_train, X_dev, Y_dev = X[:split], Y[:split], X[split:], Y[split:]

    classifier = SklearnClassifier()
    print(classifier.train(X_train, Y_train, X_dev, Y_dev))