"""
The primary file which implements the version of the null space projection.

Following steps are involved in this algorithm

Notes:
        by_class = False
        dropout_rate = 0
        min_acc = 0


The file is almost a copy of https://github.com/shauli-ravfogel/nullspace_projection/blob/master/src/debias.py
"""
import scipy
import numpy as np
from tqdm import tqdm
# from scipy.spatial import distance
from typing import List, Dict, Union

from metrics import weat
import static_db
import loader
from .classifier import SklearnClassifier
from .classifier_training_data import generate_direction, project_on_gender_subspaces, generate_x_and_y, generate_x_and_y_multiple


class NullSpaceDebias:

    def __init__(self):
        self.list_of_biased_entities = []
        self.final_projection = None

    def get_rowspace_projection(self, W: np.ndarray) -> np.ndarray:
        """
        :param W: the matrix over its nullspace to project
        :return: the projection matrix over the rowspace

        copy of https://github.com/shauli-ravfogel/nullspace_projection/blob/2403d09a5d84b3ae65129b239331a22f89ad69fc/src/debias.py#L12
        """

        if np.allclose(W, 0):
            w_basis = np.zeros_like(W.T)
        else:
            w_basis = scipy.linalg.orth(W.T)  # orthogonal basis

        P_W = w_basis.dot(w_basis.T)  # orthogonal projection on W's rowspace

        return P_W

    def get_projection_to_intersection_of_nullspaces(self, rowspace_projection_matrices: List[np.ndarray], input_dim: int):
        """
        Given a list of rowspace projection matrices P_R(w_1), ..., P_R(w_n),
        this function calculates the projection to the intersection of all nullspasces of the matrices w_1, ..., w_n.
        uses the intersection-projection formula of Ben-Israel 2013 http://benisrael.net/BEN-ISRAEL-NOV-30-13.pdf:
        N(w1)∩ N(w2) ∩ ... ∩ N(wn) = N(P_R(w1) + P_R(w2) + ... + P_R(wn))
        :param rowspace_projection_matrices: List[np.array], a list of rowspace projections
        :param dim: input dim

        copy of: https://github.com/shauli-ravfogel/nullspace_projection/blob/2403d09a5d84b3ae65129b239331a22f89ad69fc/src/debias.py#L27
        """

        I = np.eye(input_dim)
        Q = np.sum(rowspace_projection_matrices, axis=0)
        P = I - self.get_rowspace_projection(Q)
        return P

    def run(self, E, gender_direction, classifier_type='SGDClassifier', debug=False):
        """Actually implements the algorithm
        @TODO: Fill this
        """

        # gender_direction = generate_direction(E, False)
        if type(gender_direction) != list:
            gender_direction = [gender_direction] # doing this to use a generalized version where n number of direction can be given

        if debug:
            print(gender_direction[0].shape)

            print(f"the most similar word vectors are {E.model.similar_by_vector(gender_direction[0], topn=10)}")
            print(f"the most dis-similar word vectors are {E.model.similar_by_vector(-gender_direction[0], topn=10)}")

        group_set = [project_on_gender_subspaces(direction, E.model) for direction in gender_direction]
        if debug:
            print(f"top group 1 words are {group_set[0][0][:20]}")
            print(f"top group 2 words are {group_set[0][1][:20]}")
            print(f"top neutral words are {group_set[0][2][:20]}")

        X_ent, X, Y = generate_x_and_y_multiple(E, group_set)

        if debug:
            print(f"shape of X_ent is {X_ent.shape}")
            print(f"shape of X is {X.shape}")
            print(f"shape of Y is {Y.shape}")

        split = int(len(X) * .80)
        X_train, Y_train, X_dev, Y_dev = X[:split], Y[:split], X[split:], Y[split:]
        classifier = SklearnClassifier(classifier_type=classifier_type)

        final_projection, rowspace_projection_of_all_matrices, weights_of_all_classifier, acc_of_all_classifier = self.debias_projection(
            classifier=classifier, num_iter=5+5*6, X_train=X_train, Y_train=Y_train, X_dev=X_dev, Y_dev=Y_dev,
            min_accuracy=0, is_autoregressive=True
        )
        if debug:
            print(f"final projection shape is {final_projection.shape}, and accuracy matrix is {acc_of_all_classifier}")

        self.list_of_biased_entities = [i[0].lower() for g in group_set for i in g[0]+g[1]]
        # self.list_of_biased_entities = [g[0].lower() for g in group1] + [g[0].lower() for g in group2]
        self.final_projection = final_projection

        # Code to update those which are most biased and leave the remaining one

    def debias_projection(self, classifier, num_iter, X_train:np.ndarray, Y_train:np.ndarray, X_dev:np.ndarray,
                          Y_dev:np.ndarray, min_accuracy:int=0,
                          is_autoregressive:bool=True):
        """

        :param classifier_class: classifier class.
        :param num_iter: Number of iterations (number of classifier)
        :return:
        """


        if is_autoregressive == False:
            return NotImplementedError

        input_dim = X_train.shape[1]
        X_train_orig = X_train.copy()
        X_dev_orig = X_dev.copy()
        pbar = tqdm(range(num_iter))
        weights_of_all_classifier = []
        acc_of_all_classifier = []
        rowspace_projection_of_all_matrices = []

        for i in range(num_iter):
            classifier.reset()
            acc = classifier.train(X_train, Y_train, X_dev, Y_dev)
            if acc < min_accuracy:
                continue
            weights = classifier.get_weights()
            weights_of_all_classifier.append(weights)
            acc_of_all_classifier.append(acc)
            # pbar.set_description(f"iterations: {i}, and accuracy is {acc}")
            # print(f"debug acc is {acc}")
            rowspace_projection_weights = self.get_rowspace_projection(weights)
            rowspace_projection_of_all_matrices.append(rowspace_projection_weights)

            if is_autoregressive:
                projection = self.get_projection_to_intersection_of_nullspaces(rowspace_projection_of_all_matrices,
                                                                               input_dim)
                X_train = (projection.dot(X_train_orig.T)).T
                X_dev = (projection.dot(X_dev_orig.T)).T

        final_projection = self.get_projection_to_intersection_of_nullspaces(rowspace_projection_of_all_matrices,
        input_dim)
        print(f"the accuracy of classifier is {acc_of_all_classifier[-1]}")
        return final_projection, rowspace_projection_of_all_matrices, weights_of_all_classifier, acc_of_all_classifier


    def encode(self, E, words:Union[List, str]):

        def temp(word):
            if word.lower() in self.list_of_biased_entities:
                return (self.final_projection.dot(E.v(word).T)).T
            else:
                return E.v(word)

        if type(words) == str:
            return temp(words)
        else:
            words_representation = []
            for word in words:
                words_representation.append(temp(word))
            return words_representation

    def evaluate_weat_for_one_class(self, E, eval_dict: Dict, p_eval=True):
        """

        @TODO: This does not make sense here. As the whole process is supposed to debais two class like situation.
        Need to write code to evalaute two class setting.
        :param eval_dict:
        :return:

        Example of a eval_dict for class religion
        {
            ('islam_words', 'judaism_words'): [('likeable', 'unlikeable'), ('competent', 'incompetent')],
            ('islam_words', 'christianity_words'): [('likeable', 'unlikeable'), ('competent', 'incompetent')],
            ('judaism_words', 'christianity_words'): [('likeable', 'unlikeable'), ('competent', 'incompetent')]
        }
        """
        p_value , bias_value = [], []
        for categories, attributes in tqdm(eval_dict.items()):
            X = self.encode(E, static_db.get_associated_words(categories[0]))
            Y = self.encode(E, static_db.get_associated_words(categories[1]))
            for att1, att2 in attributes:
                A = self.encode(E, static_db.get_associated_words(att1))
                B = self.encode(E, static_db.get_associated_words(att2))
                if p_eval:
                    b, p = weat.compute_weat(None, X, Y, A, B, p_eval)
                    p_value.append(p)
                    bias_value.append(b)
                else:
                    b = weat.compute_weat(None, X, Y, A, B, p_eval)
                    bias_value.append(b)

        if p_eval:
            return np.mean(bias_value), np.mean(p_value)
        else:
            return np.mean(bias_value)



def evaluate_weat():
    """put this function somewhere else"""
    word_embedding = loader.WE()
    E = word_embedding.load(ename='glove-wiki-gigaword-50')
    print("WE loaded")
    # init Null space debias algorithm
    nsd = NullSpaceDebias()
    direction = generate_direction(E, False)
    nsd.run(E, direction)
    attribute_set = static_db.get_associated_words
    math = nsd.encode(E, attribute_set('math'))
    art = nsd.encode(E, attribute_set('art'))
    male = nsd.encode(E, attribute_set('male_names'))
    female = nsd.encode(E, attribute_set('female_names'))
    print(f"weat score of for male, female, math, art is {weat.compute_weat(None, male, female, math, art)}")

if __name__ == '__main__':
    evaluate_weat()
    # word_embedding = loader.WE()
    # E = word_embedding.load(ename='glove-wiki-gigaword-50')
    # print("WE loaded")
    # # init Null space debias algorithm
    # nsd = NullSpaceDebias()
    # nsd.run(E)
    # print(nsd.get_debiased_representation(E, 'dog').shape)
    # print(nsd.get_debiased_representation(E, 'sacking').shape)
    # sacking_biased = E.v('sacking')
    # sacking_unbiased = nsd.get_debiased_representation(E, 'sacking')
    # print(f"cosine distance between biased and unbiased is {distance.cosine(sacking_unbiased, sacking_biased)}")
    # print(f"bias after null space debias is {nsd.evaluate_weat_for_one_class(E, static_db.religion_eval, False)}")



