import config
from loader import FilterWordEmbeddings, WE
from double_hard_debias_utils import extract_vectors, train_and_predict, doPCA, drop

from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
import numpy as np
import operator
import scipy

class DoubleHardDebias():

    def __init__(self, class_1_words=config.hard_bias_words['class_1_words'],
                 class_2_words=config.hard_bias_words['class_2_words'],
                 class_specific_words=config.hard_bias_words['class_specific_words'],
                 definitional_pairs=config.hard_bias_words['definitional_words'],
                 bias_by_projection_list=config.hard_bias_words['bias_by_projection_list']):

        self.class_1_words = class_1_words

        self.class_2_words = class_2_words

        self.class_specific_words = class_specific_words

        self.definitional_pairs = definitional_pairs

        self.bias_by_projection_list = bias_by_projection_list

        self.main_pca = []

    @staticmethod
    def simi(a, b):
        return 1 - scipy.spatial.distance.cosine(a, b)

    def compute_bias_by_projection(self, wv, w2i,  wv_partial, w2i_partial, vocab_partial):
        """calculates bias of each word in the vocab"""
        bias_list_embed = [[wv[w2i[w1], :], wv[w2i[w2], :]] for w1, w2 in self.bias_by_projection_list]
        d = {}
        for w in vocab_partial:
            u = wv_partial[w2i_partial[w], :]
            d[w] = np.mean([self.simi(u, w1) - self.simi(u, w2) for w1,w2 in bias_list_embed])
        return d

    # get main PCA components
    def my_pca(self, wv):
        wv_mean = np.mean(np.array(wv), axis=0)
        wv_hat = np.zeros(wv.shape).astype(float)

        for i in range(len(wv)):
            wv_hat[i, :] = wv[i, :] - wv_mean

        main_pca = PCA()
        main_pca.fit(wv_hat)

        return main_pca

    def hard_debias_at_train(self,wv, w2i, w2i_partial, vocab_partial, component_ids, wv_mean):
        """
        partial here refers to set of heavily  class1 and class2 biased words, and definational words such as he,she
        """

        D = []

        for i in component_ids:
            D.append(self.main_pca.components_[i])

        # get rid of frequency features
        wv_f = np.zeros((len(vocab_partial), wv.shape[1])).astype(float)

        for i, w in enumerate(vocab_partial):
            u = wv[w2i[w], :]
            sub = np.zeros(u.shape).astype(float)
            for d in D:
                sub += np.dot(np.dot(np.transpose(d), u), d)
            wv_f[w2i_partial[w], :] = wv[w2i[w], :] - sub - wv_mean  # why subtract the wv_mean?

        # gender_directions = list()
        # for gender_word_list in self.definitional_pairs:
        #     gender_directions.append(doPCA(gender_word_list, wv_f, w2i_partial).components_[0])

        gender_directions = [doPCA(self.definitional_pairs, wv_f, w2i_partial).components_[0]]


        wv_debiased = np.zeros((len(vocab_partial), len(wv_f[0, :]))).astype(float)

        for i, w in enumerate(vocab_partial):
            u = wv_f[w2i_partial[w], :]
            for gender_direction in gender_directions:
                u = drop(u, gender_direction)
                wv_debiased[w2i_partial[w], :] = u

        return wv_debiased

    def hard_debias_at_test(self,wv, w2i, w2i_to_debias, vocab_to_debias, w2i_partial, vocab_partial, component_ids, wv_mean):
        """
        partial here refers to set of heavily  class1 and class2 biased words, and definational words such as he,she

        wv: original word vector matrix
        w2i: original word to index list

        w2i_to_debias: word to index list which are supposed to be debiased.
        vocab_to_debias: list of words which are supposed to be debiased.

        w2i_partial: word to index of list primarly consisting of list of definational pairs
        vocab_partial: list primarily consisting of definational pair

        component_ids: the freq. componenet to be removed
        """

        D = []

        for i in component_ids:
            D.append(self.main_pca.components_[i])

        # get rid of frequency features
        wv_f = np.zeros((len(w2i_to_debias), wv.shape[1])).astype(float)

        for i, w in enumerate(vocab_to_debias):
            u = wv[w2i[w], :]
            sub = np.zeros(u.shape).astype(float)
            for d in D:
                sub += np.dot(np.dot(np.transpose(d), u), d)
            wv_f[w2i_to_debias[w], :] = wv[w2i[w], :] - sub - wv_mean  # why subtract the wv_mean?

        # gender_directions = list()
        # for gender_word_list in self.definitional_pairs:
        #     gender_directions.append(doPCA(gender_word_list, wv_f, w2i_partial).components_[0])

        wv_f_definational = np.zeros((len(w2i_partial), wv.shape[1])).astype(float)
        for i, w in enumerate(vocab_partial):
            u = wv[w2i[w], :]
            sub = np.zeros(u.shape).astype(float)
            for d in D:
                sub += np.dot(np.dot(np.transpose(d), u), d)
            wv_f_definational[w2i_partial[w], :] = wv[w2i[w], :] - sub - wv_mean  # why subtract the wv_mean?

        gender_directions = [doPCA(self.definitional_pairs, wv_f_definational, w2i_partial).components_[0]]


        wv_debiased = np.zeros((len(vocab_to_debias), len(wv_f[0, :]))).astype(float)

        for i, w in enumerate(vocab_to_debias):
            u = wv_f[w2i_to_debias[w], :]
            for gender_direction in gender_directions:
                u = drop(u, gender_direction)
                wv_debiased[w2i_to_debias[w], :] = u

        return wv_debiased

    @staticmethod
    def cluster_and_visualize(X, random_state, y_true, num=2):

        kmeans = KMeans(n_clusters=num, random_state=random_state).fit(X)
        y_pred = kmeans.predict(X)
        correct = [1 if item1 == item2 else 0 for (item1, item2) in zip(y_true, y_pred)]
        preci = max(sum(correct) / float(len(correct)), 1 - sum(correct) / float(len(correct)))
        # print('precision', preci)

        return kmeans, y_pred, X, preci


    def run(self, E):
        """actually runs the process"""

        # step1 filter the embeddings
        w2i = {w: i for i, w in enumerate(E.words)}
        filtered = FilterWordEmbeddings(E.words, E.vecs, E.model)
        excluded_words = self.class_1_words + self.class_2_words + self.class_specific_words +\
                         list(set([j for i in self.definitional_pairs for j in i]))
        vocab_limit, wv_limit, _ = filtered.filter_word_embeddings(keep_name=False, keep_gendered=False,
                                                                   keep_punctuations=False,
                                                                   keep_digits=False, exclude_words=excluded_words)
        w2i_limit = {w: i for i, w in enumerate(vocab_limit)}

        # step2 compute bias of the remaining words and sort them
        bias_bef = self.compute_bias_by_projection(E.vecs, w2i, wv_limit, w2i_limit, vocab_limit)
        sorted_g = sorted(bias_bef.items(), key=operator.itemgetter(1))

        # step3 find top most biased words and least biased words (least would be more biased in the opposite direction)
        size = 1000
        class1 = [item[0] for item in sorted_g[:size]]
        class2 = [item[0] for item in sorted_g[-size:]]
        y_true = [1] * size + [0] * size # this will be used by kmeans clustering
        c_vocab = list(set(class1 + class2 + [word for words in self.definitional_pairs for word in words]))
        c_w2i = dict()
        for idx, w in enumerate(c_vocab):
            c_w2i[w] = idx

        # step4: perform PCA and centralize the vectors
        self.main_pca = self.my_pca(E.vecs)
        wv_mean = np.mean(np.array(E.vecs), axis=0)

        # step5: find the best freq. component
        precisions = []
        for component_id in range(20):
            # print('component id: ', component_id)

            wv_debiased = self.hard_debias_at_train(E.vecs, w2i, w2i_partial=c_w2i, vocab_partial=c_vocab, component_ids=[component_id],
                                      wv_mean=wv_mean)
            _, _, _, preci = self.cluster_and_visualize(
                                                   extract_vectors(class1 + class2, wv_debiased, c_w2i), 1, y_true)
            precisions.append(preci)

        minimium_precision_index = precisions.index(min(precisions))
        print(f"min prec at {minimium_precision_index}")

        # step6 debias
        # words_not_to_debias = self.class_1_words + self.class_2_words + [j for i in self.definitional_pairs for j in i]
        words_not_to_debias = [j for i in self.definitional_pairs for j in i]
        vocab_to_debias = [w for w in E.words if w not in words_not_to_debias]
        w2i_to_debias = {w: i for i, w in enumerate(vocab_to_debias)}
        vocab_partial = list(set([j for i in self.definitional_pairs for j in i]))
        # w2i_partial = {w: i for i, w in enumerate(vocab_partial)}
        w2i_partial = {}
        words_in_w2ipartial = []
        for i,w in enumerate(vocab_partial):
            if w not in words_in_w2ipartial:
                w2i_partial[w] = i
                words_in_w2ipartial.append(w)

        wv_debiased = self.hard_debias_at_test(wv = E.vecs, w2i = w2i, w2i_to_debias = w2i_to_debias,
                                     vocab_to_debias = vocab_to_debias, w2i_partial = w2i_partial,
                                     vocab_partial = vocab_partial, component_ids = [minimium_precision_index],
                                     wv_mean = wv_mean)

        print(f"final debiased shape is {wv_debiased.shape}")
        print(f"length of vocab to debias {len(vocab_to_debias)}")
        print(f"length of vocab partial {len(vocab_partial)}")

        # now need to update everything in E
        _ = E.model.most_similar('cat') # hack
        new_vecs = np.zeros((len(w2i), E.vecs.shape[1])).astype(float)
        for w in E.words:
            if w in words_not_to_debias:
                vec = E.vecs[w2i[w],:]
            else:
                vec = wv_debiased[w2i_to_debias[w], :]

            new_vecs[w2i[w]] = vec
        E.model.vectors = new_vecs
        E.vecs = new_vecs

        return E

    def encode(self, E, words):
        def temp(word):
            return E.v(word)

        if type(words) == str:
            return temp(words)
        else:
            words_representation = []
            for word in words:
                words_representation.append(temp(word))
            return words_representation



if __name__ == '__main__':
    word_embedding = WE()
    E = word_embedding.load(ename='glove-wiki-gigaword-50')
    dog_v = E.v('dog')
    d = DoubleHardDebias()
    E = d.run(E=E)
    dog_de = d.encode(E, 'dog')
    print(np.array_equal(dog_v, dog_de))


