"""Implements Bolukbasi et al. hard debias procedure.
File is a copy of
https://github.com/FEE-Fair-Embedding-Engine/FEE/blob/master/fee/debias/hard_debias.py
"""

import numpy as np

from utils import get_g_two_components
from loader import WE
import static_db

class HardDebiasBolukbasi:
    """Debiasing procedure"""

    def __init__(self, g=None):
        """HardDebias debiasing method class.

        This debiasing word vectors in two step
        stages, first it neutralizes and then equailizes the vectors.

        Args:
            E (WE class object): Word embeddings object.
            g (np.array): Gender Direction, if None, it is computed again.

        """
        if g is None:
            g = get_g_two_components(E)
        assert len(g) == E.dim
        self.g = g
        self.equalize_pairs = static_db.get_eq_pairs()

    def hard_neutralize(self, v):
        """Remove the gender component from a word vector.

        Args:
            v (np.array): Word vector.
            g (np.array): Gender Direction.

        Return:
            np.array: return the neutralized embedding

        """
        return v - self.g * v.dot(self.g) / self.g.dot(self.g)

    def neutralize(self, E, word_list):
        """Neutralize word vectors using the gender direction. This is the
        first step of hard debiasing procedure.

        Args:
            E (WE class object): Word embeddings object.
            word_list (list): List of words to debias.

        """
        for i, w in enumerate(E.words):
            if w in word_list:
                E.vecs[i] = self.hard_neutralize(E.vecs[i], self.g)
        return E

    def equalize(self, E):
        """Equalize word vectors using the gender direction and a set of
        equalizing word pairs. This is the second step of hard debiasing
        procedure.

        Args:
            E (WE class object): Word embeddings object.

        """
        g = self.g
        candidates = {x for e1, e2 in self.equalize_pairs for x in [
            (e1.lower(), e2.lower()),
            (e1.title(), e2.title()),
            (e1.upper(), e2.upper())]
                      }
        for (a, b) in candidates:
            if (a in E.index and b in E.index):
                y = self.hard_neutralize((E.v(a) + E.v(b)) / 2, g)
                z = np.sqrt(1 - np.linalg.norm(y) ** 2)
                if (E.v(a) - E.v(b)).dot(g) < 0:
                    z = -z
                E.vecs[E.index[a]] = z * g + y
                E.vecs[E.index[b]] = -z * g + y
        E.model.vectors = E.vecs
        assert E.vecs == E.model.vectors
        return E

    def encode(self, E, word_list):
        """Debias word vectors using the hard debiasing method.
        Args:
            word_list (list): List of words to debias.

        Return:
            Debiased word vectors
        """

        def temp(word):
            return E.v(word)
        if type(words) == str:
            return temp(words)
        else:
            words_representation = []
            for word in words:
                words_representation.append(temp(word))
            return words_representation


    def run(self, E):
        E = self.equalize(E)
        return E

if __name__ == '__main__':
    word_embedding = WE()
    E = word_embedding.load(ename='glove-wiki-gigaword-50')
    hardDebias = HardDebiasBolukbasi()
    print(E.v('math'))
    print(hardDebias.run([E.v('math')])