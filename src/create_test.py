"""

File reads the IAT folder in data and makes it avaiable for downstream evaluation.
The file was downloaded from https://papers.nips.cc/paper/2019/file/201d546992726352471cfea6b0df0a48-Supplemental.zip

specifcally the paper is Assessing Social and Intersectional Biases inContextualized Word Representations
"""

import json
import itertools
from glob import glob
from pathlib import Path

import static_db
from config import weat_path
from static_db import data_terms as data_terms


def generate_attributes(tests):
    attribute_sets = []
    attrs_in_attribute_set = []
    attributes = {}
    for node in tests:
        if 'IB_AAF_IB_EAM' in node['name_of_test']:
            continue
        name = node['name_of_test'].split('_')
        attr1, attr2 = name[2], name[3]
        if 'MaleTerms' == attr1 or 'MaleTerms' == attr2:
            continue
        if 'EuropeanAmericanNames' == attr1 or 'EuropeanAmericanNames' == attr2:
            continue
        if attr1 + attr2 in attrs_in_attribute_set or attr2 + attr1 in attrs_in_attribute_set:
            continue
        else:
            if attr1.lower() not in attributes.keys():
                attributes[attr1.lower()] = node['attr1']
            if attr2.lower() not in attributes.keys():
                attributes[attr2.lower()] = node['attr2']
            attribute_sets.append([attr1, attr2])
            attrs_in_attribute_set.append(attr1 + attr2)
            attrs_in_attribute_set.append(attr2 + attr1)
    return attributes, attribute_sets


def create_intersectional_bias_test():
    ib = static_db.intersectional_bias
    final_tests = []

    def create_data_dict(test_name, targ1, targ2, attr1, attr2):
        new_attr1 = [a for a in attr1 if a not in attr2]
        new_attr2 = [a for a in attr2 if a not in attr1]

        data = {
            'name_of_test': test_name,
            'attr1': new_attr1,
            'attr2': new_attr2,
            'targ1': targ1,
            'targ2': targ2,
            'file_name': "intersectional bias"
        }
        return data

    aaf, aam, eaf, eam = 'african_american_females', 'african_american_males', 'european_american_females', 'european_american_males'

    tests = [aaf, aam, eaf, eam]
    for t1, t2 in itertools.combinations(tests, 2):
        name_of_test = 'ibd_' + t1 + '_' + t2
        final_tests.append(create_data_dict(test_name=name_of_test,
                                            targ1=ib[t1],
                                            targ2=ib[t2],
                                            attr1=ib[t1 + '_ibd'],
                                            attr2=ib[t2 + '_ibd']))
    return final_tests


def clean_list(data: list):
    list_to_clean = {
        'unliked': 'unlike',
        'gladiola': 'gladiolus'
    }

    for index, w in enumerate(data):
        if w.lower() in list_to_clean.keys():
            data[index] = list_to_clean[w.lower()]
    return data


def load_json(sent_file: Path):
    ''' Load from json. We expect a certain format later, so do some post processing '''
    # log.info("Loading %s..." % sent_file)
    all_data = json.load(open(sent_file, 'r'))
    data = {}
    for k, v in all_data.items():
        examples = v["examples"]
        data[k] = examples
        v["examples"] = examples
    return all_data  # data


def read_json(file_name: Path):
    """reads the json file and makes it available for downstream tasks"""
    file = load_json(file_name)
    name_of_test = "_".join([file[key]['category'] for key in file.keys()])
    data = {
        'name_of_test': name_of_test,
        'attr1': clean_list(file['attr1']['examples']),
        'attr2': clean_list(file['attr2']['examples']),
        'targ1': clean_list(file['targ1']['examples']),
        'targ2': clean_list(file['targ2']['examples']),
        'file_name': str(file_name)
    }
    return data


def read_all_weat():
    files = glob(str(weat_path))
    data = []
    for f in files:
        if "+" in f or 'hdb':
            data.append(read_json(Path(f)))
    return data


def generate_interactive_test(attr1, attr2):
    """
    The function is not for tessting. This just generates interactive test i.e. for a given pair of attribute it returns
    all possible test combination of race and gender.

    NOT ACTUAL TEST BUT JUST REPRESTATIVE
    """
    def temp(test_name, new_attr1, new_attr2, targ1, targ2):
        data = {
            'name_of_test': test_name,
            'attr1': attributes[new_attr1.lower()],
            'attr2': attributes[new_attr2.lower()],
            'targ1': data_terms[targ1],
            'targ2': data_terms[targ2],
            'file_name': "intersectional bias"
        }
        return data

    g = ['male', 'female']
    r = ['african_american', 'european_american']

    t1 = temp(test_name='m_f', new_attr1=attr1, new_attr2=attr2, targ1=g[0] + '_names', targ2=g[1] + '_names')
    t2 = temp(test_name='aa_ea', new_attr1=attr1, new_attr2=attr2, targ1=r[0] + '_names', targ2=r[1] + '_names')

    t3 = temp(test_name='aam_eam', new_attr1=attr1, new_attr2=attr2, targ1=r[0] + '_' + g[0] + '_names',
              targ2=r[1] + '_' + g[0] + '_names')

    t4 = temp(test_name='aaf_eaf', new_attr1=attr1, new_attr2=attr2, targ1=r[0] + '_' + g[1] + '_names',
              targ2=r[1] + '_' + g[1] + '_names')

    t5 = temp(test_name='aam_aaf', new_attr1=attr1, new_attr2=attr2, targ1=r[0] + '_' + g[0] + '_names',
              targ2=r[0] + '_' + g[1] + '_names')

    t6 = temp(test_name='eam_eaf', new_attr1=attr1, new_attr2=attr2, targ1=r[1] + '_' + g[0] + '_names',
              targ2=r[1] + '_' + g[1] + '_names')

    t7 = temp(test_name='aam_eaf', new_attr1=attr1, new_attr2=attr2, targ1=r[0] + '_' + g[0] + '_names',
              targ2=r[1] + '_' + g[1] + '_names')

    t8 = temp(test_name='aaf_eam', new_attr1=attr1, new_attr2=attr2, targ1=r[0] + '_' + g[1] + '_names',
              targ2=r[1] + '_' + g[0] + '_names')



    return [t1, t2, t3, t4, t5, t6, t7, t8]


attributes, attribute_sets = generate_attributes(tests=read_all_weat())
print(attribute_sets)
if __name__ == '__main__':
    # path = Path('../data/iats/weat1.jsonl')
    # print(load_json(path))
    # print(read_all_weat())
    # print(create_intersectional_bias_test())
    print(generate_interactive_test(attr1='pleasant', attr2='unpleasant'))
