"""Saves multiple configuration as well as data paths"""
import json
from pathlib import Path

import os

path = os.getcwd()

def read_txt(path:Path):
    """
    reads a text file line by line.
    """
    with open(path, "r") as f:
        lines = f.readlines()

    text = [l.strip().lower() for l in lines]
    return text

ROOT_LOC: Path = Path('..') if str(Path().cwd()).split('/')[-1] == 'src' else Path('.')


# List of all gender specific words. More information in README.MD of data specific folder
gender_specific_gonen = json.load(open(ROOT_LOC / Path('data/gonen/gender_specific_full.json')))

# List of all first name. More information in README.MD of the data specific folder
first_name_gonen = read_txt(path=ROOT_LOC / Path('data/gonen/first_names_clean.txt'))


sgd_classifier = {'loss': 'hinge',
                  'max_iter': 1000,
                  'early_stopping': True,
                  'tol': 1e-3}

linear_svc = {'max_iter': 2000}


weat_path = ROOT_LOC / Path('data/iats/weat*')

hard_bias_words = {
    'class_1_words': read_txt(ROOT_LOC / Path('data/double_hard_debias/male_word_file.txt')),
    'class_2_words': read_txt(ROOT_LOC / Path('data/double_hard_debias/female_word_file.txt')),
    'class_specific_words': json.load(open(ROOT_LOC / Path('data/double_hard_debias/gender_specific_full.json'))),
    'definitional_words': [['she', 'he'], ['herself', 'himself'], ['her', 'his'], ['daughter', 'son'],
                      ['girl', 'boy'], ['mother', 'father'], ['woman', 'man'], ['mary', 'john'],
                      ['gal', 'guy'], ['female', 'male']],
    'bias_by_projection_list': [['he', 'she']]
}