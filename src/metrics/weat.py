"""
Actually does the WEAT evaluation. The file is same as
https://github.com/FEE-Fair-Embedding-Engine/FEE/blob/master/fee/metrics/weat.py
"""

# from src \
import loader
import static_db

import numpy as np
from sympy.utilities.iterables import multiset_permutations
from sklearn.metrics.pairwise import cosine_similarity
from itertools import combinations
import random


def unit_vector(vec):
    """
    Returns unit vector
    """
    return vec / np.linalg.norm(vec)


def cos_sim(v1, v2):
    """
    Returns cosine of the angle between two vectors
    """
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return np.clip(np.tensordot(v1_u, v2_u, axes=(-1, -1)), -1.0, 1.0)


def weat_association(W, A, B):
    """
    Returns association of the word w in W with the attribute for WEAT score.
    s(w, A, B)
    :param W: target words' vector representations
    :param A: attribute words' vector representations
    :param B: attribute words' vector representations
    :return: (len(W), ) shaped numpy ndarray. each rows represent association of the word w in W
    """
    # return np.mean(cosine_similarity(W, A), axis=-1) - np.mean(cosine_similarity(W, B), axis=-1)
    return np.mean(cos_sim(W, A), axis=-1) - np.mean(cos_sim(W, B), axis=-1)


def weat_differential_association(X, Y, A, B):
    """
    Returns differential association of two sets of target words with the attribute for WEAT score.
    s(X, Y, A, B)
    :param X: target words' vector representations
    :param Y: target words' vector representations
    :param A: attribute words' vector representations
    :param B: attribute words' vector representations
    :return: differential association (float value)
    """
    return np.sum(weat_association(X, A, B)) - np.sum(weat_association(Y, A, B))


def weat_p_value(X, Y, A, B, n_samples=10000):
    """
    Returns one-sided p-value of the permutation test for WEAT score
    CAUTION: this function is not appropriately implemented, so it runs very slowly
    :param X: target words' vector representations
    :param Y: target words' vector representations
    :param A: attribute words' vector representations
    :param B: attribute words' vector representations
    :return: p-value (float value)
    """
    diff_association = weat_differential_association(X, Y, A, B)
    target_words = np.concatenate((X, Y), axis=0)

    # get all the partitions of X union Y into two sets of equal size.
    idx = np.zeros(len(target_words))
    idx[:len(target_words) // 2] = 1

    partition_diff_association = []
    counter = 0
    # i_array = [i for i in multiset_permutations(idx)]
    # random.shuffle(i_array)
    for _ in range(n_samples):
        random.shuffle(idx)
        i = np.array(idx, dtype=np.int32)
        partition_X = target_words[i]
        partition_Y = target_words[1 - i]
        partition_diff_association.append(weat_differential_association(partition_X, partition_Y, A, B))
        counter = counter + 1
        if counter > n_samples:
            break
    partition_diff_association = np.array(partition_diff_association)

    return np.sum(partition_diff_association > diff_association) / len(partition_diff_association)


def random_permutation(iterable, r=None):
  """Returns a random permutation for any iterable object"""
  pool = tuple(iterable)
  r = len(pool) if r is None else r
  return tuple(random.sample(pool, r))


# def weat_p_alternate(X,Y,A,B,sample=1000):
#     """an alternate implemetnation of the above function"""
#     size_of_permutation = min(len(X), len(Y))
#     X_Y = np.concatenate((X, Y), axis=0)
#
#     if not sample:
#         permutations = combinations(X_Y, size_of_permutation)
#     else:
#         permutations = [random_permutation(X_Y, size_of_permutation) for s in range(sample)]
#
#     for Xi in permutations:
#         Yi = filterfalse(lambda w: w in Xi, X_Y)
#         Ximat = np.array([embd[w.lower()] for w in Xi if w.lower() in embd])
#         Yimat = np.array([embd[w.lower()] for w in Yi if w.lower() in embd])
#         test_stats_over_permutation.append(test_statistic(Ximat, Yimat, Amat, Bmat))



def weat_score(X, Y, A, B, p_val):
    """
    Returns WEAT score
    X, Y, A, B must be (len(words), dim) shaped numpy ndarray
    CAUTION: this function assumes that there's no intersection word between X and Y
    :param X: target words' vector representations
    :param Y: target words' vector representations
    :param A: attribute words' vector representations
    :param B: attribute words' vector representations
    :return: WEAT score
    """

    x_association = weat_association(X, A, B)
    y_association = weat_association(Y, A, B)

    tmp1 = np.mean(x_association, axis=-1) - np.mean(y_association, axis=-1)
    tmp2 = np.std(np.concatenate((x_association, y_association), axis=0))

    # tmp2 = np.std(weat_association(np.concatenate((X, Y), axis=0), A, B))
    w_score = tmp1 / tmp2

    if p_val:
        p = weat_p_value(X, Y, A, B)
        return w_score, p

    return w_score


def compute_weat(E, target_1, target_2, attributes_1, attributes_2, p_eval=True):
    """

    target_1 is a list of wordvectos (numpy). Similiar for all other params too

    if E is none than all sets are assumed to be embedded
    """
    if E:
        attributes_1 = [E.v(word.lower()) for word in attributes_1]
        attributes_2 = [E.v(word.lower()) for word in attributes_2]
        target_1 = [E.v(word.lower()) for word in target_1]
        target_2 = [E.v(word.lower()) for word in target_2]
        # return weat_score(target_1_vec, target_2_vec, attributes_1_vec, attributes_2_vec, p_eval)
    return weat_score(target_1, target_2, attributes_1, attributes_2, p_eval)


if __name__ == '__main__':
    # Get set of attributes and target words
    attribute_set = static_db.get_weat_attribute_set()
    math = attribute_set['math']
    art = attribute_set['art']
    male = attribute_set['male_2']
    female = attribute_set['female_2']



    print(male, female, math, art)
    # Embed them
    word_embedding = loader.WE()
    E = word_embedding.load(ename='glove-wiki-gigaword-50')


    # Calculate WEAT
    w_score= compute_weat(E=E, target_1=female, target_2=male, attributes_1=art,
                              attributes_2=math, p_eval=True)

    # print scores
    print(f"the weat score is {w_score} and the one-sided p value is NA")

