"""
A set of utility functions which are useful across the board. The file is same as
https://github.com/FEE-Fair-Embedding-Engine/FEE/blob/master/fee/utils.py
"""
import static_db

import string
import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA

def doPCA_two_components(pairs, embedding, num_components = 10, plot=False):
    matrix = []
    for a, b in pairs:
        center = (embedding.v(a) + embedding.v(b))/2
        matrix.append(embedding.v(a) - center)
        matrix.append(embedding.v(b) - center)
    matrix = np.array(matrix)
    pca = PCA(n_components = num_components)
    pca.fit(matrix)
    if plot:
        plt.bar(range(num_components), pca.explained_variance_ratio_)
    return pca

def get_g_two_components(E, definitional=None):
    if definitional is None:
        definitional = static_db.get_gender_definitional()
    g = doPCA_two_components(definitional, E).components_[0]
    return g


def has_punct(w: str):
    """
    :param w: a word
    :return: True if contains punctuation, False otherwise
    """
    if any([c in string.punctuation for c in w]):
        return True
    return False


def has_digit(w: str):
    """
    :param w: a word
    :return: True if contains digit, False otherwise
    """
    if any([c in '0123456789' for c in w]):
        return True
    return False


def combine_two_dictionaries(dict1, dict2):
    temp = {}
    for key in set().union(dict1, dict2):
        if key in dict1: temp.setdefault(key, []).extend(dict1[key])
        if key in dict2: temp.setdefault(key, []).extend(dict2[key])

    # Create final dictionary without repeating items in value.
    new_dict = {}
    for key, value in temp.items():
        new_dict[key] = list(set(value))

    return new_dict

if __name__ == '__main__':
    import loader
    # from src import loader
    word_embedding = loader.WE()
    E = word_embedding.load(ename='glove-wiki-gigaword-50')
    print(get_g_two_components(E))