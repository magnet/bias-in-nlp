"""
File which has classes and function used to load the embedding file. The file is based on
https://github.com/FEE-Fair-Embedding-Engine/FEE/blob/master/fee/embedding/loader.py
"""

import gensim.downloader as api
from typing import List
from tqdm import tqdm
import numpy as np
import codecs

import config
from utils import has_digit, has_punct


class WE:
    """The Word embedding class.
    The main class that facilitates the word embedding structure.
    Attributes
    ----------
    dim (int): Dimension of embedding
    vecs (np.array):
    """

    def __init__(self):
        """
        Initialize WE object.
        """
        # self.downloader = Downloader()
        self.desc = "Word embedding loader for "

    def fname_to_format(self, fname):
        """Get embedding format from file name.
        Format can usually be extracted from the filename extension. We
        currently support the loading of embeddings in binary (.bin),
        text (.txt) and numpy format (.npy).

        Args:
            fname (str): file name

        Return:
            format (str): format (txt, bin or npy)
        """

        if fname is None:
            raise Exception("fname can't be None")
            return None

        if fname.endswith('.txt'):
            format = 'txt'
        elif fname.endswith('.bin'):
            format = 'bin'
        else:
            format = 'npy'
        return format

    def get_gensim_word_vecs(self, model):
        """ Loading word and vecs using gensim scripts.
        Args:
            model (gensim object): Model for accessing all the words in
                                   vocab, and their vectors.

        """
        words = sorted([w for w in model.vocab], key=lambda w: model.vocab[w].index)
        vecs = np.array([model[w] for w in words])
        return words, vecs

    def _load(self, fname, format, dim=300):
        """Internal load function.
        There shall be no exceptions in this function. Verify everything
        beforehand. Loads word embedding at location `fname` on disk.
        Args:
            fname (str): Path to the embedding file on disk.
            format (str): Format of word embedding. Following are the
                          supported formats:
                            - binary
                            - text
                            - numpy array
            dim (int): The dimension of embedding vectors.

        Return:
            words (list): List of vocabulary words.
            vecs (np.array): Word vectors of size (self.n, self.dim)
            model (gensim.KeyedVectors or None): genism word vecor model is it ends with .bin else None
        """
        vecs = []
        words = []
        model = None  # This is with gensm word_vector model.

        if format is None:
            format = self.fname_to_format(fname)

        if format == 'bin':
            import gensim.models
            model = gensim.models.KeyedVectors.load_word2vec_format(fname, binary=True)
            words, vecs = self.get_gensim_word_vecs(model)

        elif format == 'txt':
            with open(fname, "r") as f:
                lines = f.readlines()
                for line in lines:
                    tokens = line.split()
                    v = np.array([float(x) for x in tokens[-dim:]])
                    w = "_".join([str(x) for x in tokens[:-dim]])
                    if len(v) != dim:
                        print(f"Weird line: {tokens} | {len(v)}")
                        continue
                    words.append(w)
                    vecs.append(v)

        else:
            with codecs.open(fname + '.vocab', 'r',
                             'utf-8') as f_embed:
                words = [line.strip() for line in f_embed]
            vecs = np.load(fname + '.wv.npy')

        self.n, self.dim = vecs.shape

        self.desc = f"File: {fname}\tFormat: {format}\t" \
                    f"#Words: {self.n}\tDimension: {self.dim}"
        return words, vecs, model

    def load(self, fname=None, format=None, ename=None,
             normalize=False, dim=300):
        """Load word embedding from filename or embedding name.
        Loads word embeddings from either filename `fname` or the
        embedding name `ename`. Following formats are supported:
        - bin: Binary format, load through gensim.
        - txt: Text w2v or GloVe format.
        - npy: Numpy format. `fname.wv.npy` contans the numpy vector
               while `fname.vocab` contains the vocabulary list.
        All Gensim pre-trained embeddings are integrated for easy access
        via `ename`. `ename` are same as the gensim conventions.
        The gensim KeyedVector model is also avaialble in case of using gensim model or via .bin extension
        Example:
            ```
            we = WE()
            E = we.load('glove6B.txt', dim = 300)
            ```
            ```
            we = WE()
            E = we.load(ename = 'glove-wiki-gigaword-50')
            ```
        Args:
            fname (str): Path to the embedding file on disk.
            format (str): Format of word embedding. Following are the
                          supported formats:
                            - binary
                            - text
                            - numpy array
            ename (str): Name of embedding. This will download embedding
                         using the `Downloader` class. In case both
                         ename and fname are provided, ename is given
                         priority.
            normalize (bool): Normalize word vectors or not.
            dim (int): The dimension of embedding vectors.
                       Default dimension is 300
        Return:
            self (WE object): Return self, the word embedding object.

        """
        if ename is not None:
            model = api.load(ename)
            words, vecs = self.get_gensim_word_vecs(model)

        else:
            words, vecs, model = self._load(fname, format, dim)

        self.words = words
        self.vecs = vecs
        self.model = model
        self.reindex()
        self.normalized = normalize
        if normalize:
            self.normalize()
        return self

    def reindex(self):
        """
        Reindex word vectors.
        """
        self.index = {w: i for i, w in enumerate(self.words)}
        self.n, self.dim = self.vecs.shape
        assert self.n == len(self.words) == len(self.index)

    def v(self, word):
        """Access vector for a word
        Returns the `self.dim` dimensional vector for the word `word`.
        Example:
            E = WE().load('glove')
            test = E.v('test')
        Args:
            word (str): Word to access vector of.
        Return:
            vec (np.array): `self.dim` dimension vector for `word`.

        """
        # What if the word is not found.
        # @TODO: add a mechanism to return a random or a zero vector if the word is not found.
        try:
            if " " not in word:
                vec = self.vecs[self.index[word.lower()]]
            else:
                temp = [self.vecs[self.index[w.strip().lower()]] for w in word.split(" ")]
                return np.mean(temp,0)
        except:
            Warning("word not found")
            print(f"{word} not found in the dictionary")
            vec = np.zeros_like(self.vecs[self.index['dog']])
        return vec

    def normalize(self):
        """Normalize word embeddings.
        Normaliation is done as follows:
            \vec{v}_{norm} := \vec{v}/|\vec{v}|
            where |\vec{v}| is the L2 norm of \vec{v}

        """
        self.vecs /= np.linalg.norm(self.vecs, axis=1)[:, np.newaxis]
        self.reindex()
        self.desc += "\tNormalized: True"
        self.normalized = True

    def __repr__(self):
        """Class `__repr__` object for pretty informational print.

        """
        return self.desc


class FilterWordEmbeddings:

    def __init__(self, words, vecs, model=None):
        self.words = words
        self.vecs = vecs
        self.model = model

    def filter_word_embeddings(self, keep_name:bool = False, keep_gendered: bool = False, keep_punctuations:bool= False,
                               keep_digits:bool = False, exclude_words: List = []):
        """
        Following filters can be applied used
        >> keep_name: If false, removes names from word embeddings\
        >> keep_gendered: If false, removes inherently gendered words from the embeddings
        >> keep_punctuations: if false, removes punctuations from the embeddings
        >> keep_digits: if false, removes digits from the embeddings
        """

        words_to_remove = []
        if keep_name:
            name = []
        else:
            name = config.first_name_gonen

        if keep_gendered:
            gendered = []
        else:
            gendered = config.gender_specific_gonen

        for word in self.words:
            if (not keep_punctuations) and has_punct(word):
                words_to_remove.append(word)
            if (not keep_digits) and has_digit(word):
                words_to_remove.append(word)

        words_to_remove = words_to_remove + gendered + name
        # words_to_remove = list(set(words_to_remove))
        if exclude_words:
            words_to_remove = words_to_remove + list(set(exclude_words))
        words_to_remove = list(set(words_to_remove))

        if self.model: # model is not none
            self.model = self.remove_words_w2v(self.model, words_to_remove)
            # self.words = sorted([w for w in self.model.vocab], key=lambda w: self.model.vocab[w].index)
            self.words = self.model.index2entity
            self.vecs = np.array([self.model[w] for w in self.words])
        else:
            self.words = [word for word in self.words if word not in words_to_remove]
            self.vecs = np.array([self.model[w] for w in self.words])

        return self.words, self.vecs, self.model


    def remove_words_w2v(self, w2v, remove_words:List[str]):
        """
        :param w2v: gensim keyvalue model
        :param remove_words: list of words to be removed from the model
        :return: updated w2v
        Function inspired from https://stackoverflow.com/a/55725093
        """
        _ = w2v.most_similar('cat') # A hack. Don't remove it.
        remove_words = [word.lower() for word in remove_words]
        entity2index = {value:key for key,value in enumerate(w2v.index2entity)}
        index_of_words_to_remove = [entity2index[word.lower()] for word in remove_words if word.lower() in w2v.vocab]
        index_of_words_to_keep = list(set([i for i in range(len(w2v.vocab))]) - set(index_of_words_to_remove))


        entity_to_keep = [w2v.index2entity[i] for i in index_of_words_to_keep]

        new_vecs = w2v.vectors[index_of_words_to_keep]
        new_vec_norm = w2v.vectors_norm[index_of_words_to_keep]

        new_vocab = {}
        for index, ent in tqdm(enumerate(entity_to_keep)):
            key = ent
            value = w2v.vocab[key]
            value.index = index
            new_vocab[key] = value

        w2v.vocab = new_vocab
        w2v.vectors = new_vecs
        w2v.index2entity = entity_to_keep
        w2v.index2word = entity_to_keep
        w2v.vectors_norm = new_vec_norm

        return w2v


if __name__ == "__main__":
    word_embedding = WE()
    E = word_embedding.load(ename='glove-wiki-gigaword-50')
    E.model.vocab['dog']
    print(f"The embeddings of the word nurse is {E.v('nurse')}")
    print(f"The most similar word to Nurse via gensim is {E.model.similar_by_word('nurse')[0]}")
    print(f"length of vocab is {len(E.words)}")
    filter = FilterWordEmbeddings(E.words, E.vecs, E.model)
    E.words, E.vecs, E.model = filter.filter_word_embeddings(keep_name= False, keep_gendered = False, keep_punctuations= False,
                               keep_digits= False)
    E.reindex()
    print(f"length of vocab is {len(E.words)}")
