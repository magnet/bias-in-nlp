"""Basic form of evaluation"""
from functools import partial


from loader import WE
from debias.conceptor import Conceptor
from debias.nullprojection.null_projection import NullSpaceDebias
from debias.nullprojection.classifier_training_data import generate_direction
from metrics import weat
from static_db import get_associated_words


# sys.path.append('debias/null_projection')
def conceptor_encoder_function(E, r_words):
    conceptor = Conceptor()
    conceptor.run(E, r_words)
    return partial(conceptor.encode,E)

def null_projection_encoder_function(E, gender_direction):
    nsd = NullSpaceDebias()
    # nsd.run(E, gender_direction, 'linearsvc')
    nsd.run(E, gender_direction, 'sgdclassifier')
    return partial(nsd.encode, E)

# Step1: Load word Embedding
word_embedding = WE()
E = word_embedding.load(ename='glove-wiki-gigaword-50')

def evaluate_weat(encoder_function, att1, att2, x, y):
    att1 = encoder_function(att1)
    att2 = encoder_function(att2)
    x = encoder_function(x)
    y = encoder_function(y)
    return weat.compute_weat(None, x, y, att1, att2)

# calculatinf direction for npe and list of words for ce
r_words = get_associated_words('male_names') + get_associated_words('female_names')
direction = generate_direction(E, False)

npe = null_projection_encoder_function(E, direction)
ce = conceptor_encoder_function(E, r_words)

print(f"for conceptor in case of weat score of for male, female, math, art is"
      f" {evaluate_weat(ce, get_associated_words('math'),get_associated_words('art'), get_associated_words('male_names'), get_associated_words('female_names'))}")

print(f"for null space project in case of weat score of for male, female, math, art is"
      f" {evaluate_weat(npe, get_associated_words('math'),get_associated_words('art'), get_associated_words('male_names'), get_associated_words('female_names'))}")

print("NOTE: The scores would be different when male_names vs male same case for female_names vs female")

"""
Step1> Debias 
"""