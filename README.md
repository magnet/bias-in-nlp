# Bias In Nlp

Some experiments to detect bias in NLP models. 

The code base is heavily inspired from the following excellent repositories:
1) [An-Evaluation-of-Multiclass-Debiasing-Methods-on-Word-Embeddings](https://github.com/thaleaschlender/An-Evaluation-of-Multiclass-Debiasing-Methods-on-Word-Embeddings)
2) [Fair Embedding Engine](https://github.com/FEE-Fair-Embedding-Engine/FEE)


## TODO
1. Refactor the function evaluate_weat_for_one_class in conceptor.py